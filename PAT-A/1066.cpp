/* Birdy&C 2017.3.3
1066. Root of AVL Tree (25)*/
#include<iostream>
using namespace std;

class AvlNode
{
public:
	AvlNode *Left;
	AvlNode *Right;
	int Element;
	AvlNode() : Left(NULL), Right(NULL), Element() {}
	AvlNode(int ele) :Left(NULL), Right(NULL), Element(ele) {}
	void print();
	int height();
	AvlNode *SingleRotL();
	AvlNode *SingleRotR();
	AvlNode *DoubleRotLR();
	AvlNode *DoubleRotRL();

};

class AvlTree
{
public:
	AvlNode *HeadNode;

	AvlTree() : HeadNode(NULL)
	{
	}

	~AvlTree()
	{
		if (HeadNode != NULL)
			delete HeadNode;
	}

	AvlNode* Insert(int t);

	AvlNode* Insert(int t, AvlNode * node);

	void print()
	{
		if (NULL == HeadNode)
		{
		}
		else
		{
			HeadNode->print();
		}

	}

	void print_head()
	{
		if (NULL == HeadNode)
		{
		}
		else
		{
			printf("%d", HeadNode->Element);
		}

	}
};

AvlNode* AvlTree::Insert(int t)
{
	HeadNode = Insert(t, HeadNode);
	return HeadNode;
}

AvlNode* AvlTree::Insert(int t, AvlNode* node)
{
	if (node == NULL) {
		return node = new AvlNode(t);
	}
	else if (t < node->Element)
	{
		node->Left = Insert(t, node->Left);
		int left = 0, right = 0;
		if (node->Left != NULL) left = node->Left->height();
		if (node->Right != NULL) right = node->Right->height();

		if (left - right == 2)
		{
			if (t < node->Left->Element)
			{
				node = node->SingleRotL();
			}
			else
			{
				node = node->DoubleRotLR();
			}
		}
	}
	else if (t > node->Element)
	{
		node->Right = Insert(t, node->Right);
		int left = 0, right = 0;
		if (node->Left != NULL) left = node->Left->height();
		if (node->Right != NULL) right = node->Right->height();

		if (left - right == -2)
		{
			if (t > node->Right->Element)
			{
				node = node->SingleRotR();
			}
			else
			{
				node = node->DoubleRotRL();
			}
		}
	}
	return node;
}

void AvlNode::print()
{
	if (NULL != Left)
	{
		Left->print();
	}
	if (NULL != Right)
	{
		Right->print();
	}
}

int AvlNode::height()
{
	if (NULL == Left && NULL == Right) { return 1; }
	if (NULL == Left) { return Right->height() + 1; }
	if (NULL == Right) { return Left->height() + 1; }
	int R = Right->height();
	int L = Left->height();
	return (R > L ? R : L) + 1;
}

AvlNode *AvlNode::SingleRotL()
{
	AvlNode *temp;
	temp = Left;
	Left = temp->Right;
	temp->Right = this;
	return temp;

}

AvlNode *AvlNode::SingleRotR()
{
	AvlNode *temp;
	temp = Right;
	Right = temp->Left;
	temp->Left = this;
	return temp;
}

AvlNode *AvlNode::DoubleRotLR()
{
	Left = Left->SingleRotR();
	return SingleRotL();
}

AvlNode *AvlNode::DoubleRotRL()
{
	Right = Right->SingleRotL();
	return SingleRotR();
}




int main(void)
{
	AvlTree Tree;
	int n;
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
	{
		int t;
		scanf("%d", &t);
		Tree.Insert(t);
	}


	Tree.print_head();
	system("pause");
	return 0;
}
