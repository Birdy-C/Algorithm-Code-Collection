#include<iostream>
#include<string>
#include<vector>
#include<algorithm>

using namespace std;


class table 
{
public:
	int overtime;
	bool isVIP;
	int times;
	int index;
	bool operator < (const table &m)const 
	{
		if (overtime == m.overtime)
		{
			//这里 居然所有点都超时……
			if (isVIP != m.isVIP)
			{
				return isVIP;
			}
			return index < m.index;
		}
		return overtime < m.overtime;
	}
	table(int index_t) :overtime(8 * 60 * 60), isVIP(false), times(0), index(index_t)
	{
	}
};

class customer
{
public:
	int arrivetime;
	int playtime;
	bool isVIP;
	customer(int arrivetime_t, int playtime_t, bool isVIP_t)
	{
		arrivetime = arrivetime_t;
		playtime = playtime_t;
		isVIP = isVIP_t;
	}

	bool operator < (const customer &m)const
	{
		return arrivetime < m.arrivetime;
	}

};

bool VIPless(class customer cum1, class customer cum2)
{
	if (cum1.isVIP != cum2.isVIP)
	{
		return cum1.isVIP;
	}
	return cum1.arrivetime < cum2.arrivetime;
}

int timechange(string time)
{
	return stoi(time.substr(0, 2)) * 3600 + stoi(time.substr(3, 2)) * 60 + stoi(time.substr(6, 2));
	
}

void timeprint(int time)
{
	printf("%02d:%02d:%02d", time / 3600, time / 60 % 60, time % 60);
}


int main()
{
	int N;
	cin >> N;
	vector<customer> customer_all;
	string arrivetime;
	int playtime;
	bool vip;
	for (int i = 0; i < N; i++)
	{
		cin >> arrivetime >> playtime >> vip;
		// 2h at most  test point 4
		customer t_customer(timechange(arrivetime), min(playtime * 60, 2 * 60 * 60), vip); // min到sec的转换 *60
		customer_all.push_back(t_customer);
	}
	int K, M, temp;
	cin >> K >> M;

	std::vector<table> tables;
	for (int i = 0; i < K; i++)
	{
		table t_table(i);
		tables.push_back(t_table);
	}

	for (int i = 0; i < M; i++)
	{
		cin >> temp;
		tables[temp-1].isVIP = true;//numer 居然对应从0开始……
	}



	int time = timechange("08:00:00");
	int timeend = timechange("21:00:00");

	//std::sort(std::begin(customer_all), std::end(customer_all));
	std::vector<customer>::iterator first_customer;
	std::vector<table>::iterator min_table;
	std::vector<customer>::iterator first_vip;
	std::vector<customer>::iterator servered;
	while (1)
	{
		if (customer_all.size() == 0)
		{
			break; // 客户队列为空
		}

		min_table = std::min_element(std::begin(tables), std::end(tables));
		if (min_table->overtime >= timeend)
		{
			break; // 结束过晚
		}
		first_customer = std::min_element(std::begin(customer_all), std::end(customer_all));
		first_vip = std::min_element(std::begin(customer_all), std::end(customer_all),VIPless);

		if (first_customer->arrivetime >= timeend)
		{
			break; // 客户队列为空
		}


		if (min_table->overtime <= first_customer->arrivetime)
		{
			servered = first_customer;
			//用户到的时候有桌子空着 不是vip取编号最小的
			for (std::vector<table>::iterator it = std::begin(tables); it < std::end(tables); it++)
			{
				if (it->overtime <= first_customer->arrivetime)
				{
					min_table = it;
					break;
				}
			}
			//是 vip 并且有空的vip桌 取vip桌  Test Point 3 7
			if (first_customer->isVIP)
			{
				for (std::vector<table>::iterator it = std::begin(tables); it < std::end(tables); it++)
				{
					if (it->overtime <= first_customer->arrivetime && it->isVIP)
					{
						min_table = it;
						break;
					}
				}
			}
			/*
			else
			{
				//不需要考虑把vip桌子空出来……vip真惨
				for (std::vector<table>::iterator it = std::begin(tables); it < std::end(tables); it++)
				{
					if (it->overtime <= first_customer->arrivetime && !it->isVIP)
					{
						min_table = it;
						break;
					}
				}
			}
			*/
		}
		else if (true == min_table->isVIP && true == first_vip->isVIP && first_vip->arrivetime <= min_table->overtime)
		{
			servered = first_vip;
		}
		else
		{
			servered = first_customer;
		}
		/*
		else if (false == min_table->isVIP|| 
			false == first_vip->isVIP || (true == first_vip->isVIP && first_vip->arrivetime >= min_table->overtime))
		{
			//不是vip桌子  或者 队列里没有vip 或者 桌子空出来的时候vip还没到
			servered = first_customer;
		}
		//if (true == min_table->isVIP && true == first_vip->isVIP && first_vip->arrivetime <= min_table->overtime)
		else
		{
			servered = first_vip;
		}
		*/

		timeprint(servered->arrivetime);
		timeprint(max(servered->arrivetime, min_table->overtime));
		int waiting_time = (30 + max(0, min_table->overtime - servered->arrivetime)) / 60;//test 8 四舍五入
		cout << waiting_time << endl;

		min_table->times++;
		min_table->overtime = max(servered->arrivetime, min_table->overtime) + servered->playtime;

		customer_all.erase(servered);
	} 

	for (std::vector<table>::iterator it = std::begin(tables); it < std::end(tables); it++)
	{
		if(it == std::begin(tables))
			cout  << it->times;
		else
			cout <<' '<< it->times ;
	}
	system("pause");
	return 0;
}