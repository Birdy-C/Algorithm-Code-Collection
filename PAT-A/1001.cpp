/*
@author Birdy 18.2.18 
*/
# include <iostream>
using namespace std;

/*
-1000000 <= a, b <= 1000000
*/

// number - 0~999
//flag true - add to 3 number with 0 in the form
void print1000(int number, bool & flag)
{
	if (!flag && number)
	{
		cout << number;
		flag = true;
	}
	else if(flag)
	{
		int m = number, count = 0;
		while (m = m / 10)
		{
			count++;
		}
		for (int i = 1; i < 3 - count; i++)
		{
			cout << '0';
		}
		cout << number;
	}
	if (number)
		flag = true;

}



int main(void)
{
	int number1, number2;
	cin >> number1 >> number2;
	int result = number1 + number2;

	if (result < 0)
	{
		cout << '-'; result = -result;
	}
	if (result == 0)
	{
		cout << '0';
		return 0;
	}

	int temp = result / 1000000;
	bool flag = false;
	print1000(result / 1000000, flag);
	if (flag) cout << ',';
	print1000((result / 1000) % 1000, flag);
	if (flag) cout << ',';
	print1000(result % 1000, flag);
	system("pause");
	return 0;

}
