// @author Birdy 2018.3.4
/*

1060
���뿪ͷΪ0
Test Point 3  2 0.00001 0.00000001
Test Point 6  0.0000 
*/
#include<iostream>
#include<string>
using namespace std;

string remove0(string M)
{
	// remove 0 with no meanings
	int i = 0;
	string t;
	bool flag = true;
	while (i < M.length())
	{
		if (M.at(i) != '0')
		{
			t = M.substr(i);
			break;
		
		}
		i++;
	}
	int len = t.length();
	for (int j = t.length() - 1; j >= 0; j--)
	{
		char num = t.at(j); 
		if (num == '0'&&flag)
		{
			len--;
		}
		else if (num != '0')
		{
			flag = false;
		}

		if (t.at(j) == '.')
		{
			return t.substr(0, len);			
		}
	}
	return t;
}



void print_char(char m, int& num, int N, string &result)
{
	if (num < N)
	{
		num++;
		result += m;
	}
}

string printnumber(string M, int N)
{
	//space in the front
	M = remove0(M);
	string result(" 0.");
	int x = 0, i = 0, num_print = 0;
	bool flag = false;
	bool beforepoint = true;
	while (num_print < N || i < M.length())
	{
		if (M.length() <= i)
		{
			print_char('0', num_print, N, result);
		}
		else
		{
			char number = M.at(i);
			if ('0' != number && '.' != number)
			{ 
				flag = true;
			}

			if ('.' == number)
			{
				beforepoint = false;
			}
			else if (flag)
			{
				print_char(number, num_print, N, result);
				beforepoint ? x++ : 0;
			}
			else 
			{
				beforepoint ? 0 : x--;
			}
		}
		i++;
	}
	result += "*10^";
	result += to_string(x);
	return result;

}

int main()
{
	int N;
	string A, B;
	// what's the use of N?
	cin >> N >> A >> B;
	A = printnumber(A, N);
	B = printnumber(B, N);
	if (A == B)
	{
		cout << "YES"<<A;
	}
	else
	{
		cout << "NO"<<A<<B;
	}
	system("pause");
	return 0;
}
