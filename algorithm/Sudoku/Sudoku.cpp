#include "iostream"
#include "string"
#include "Sudoku.h"
#include <fstream>

const int ALLDIGITS = (1 << 9) - 1;
int calDigits[ALLDIGITS + 1] = { 0 };	//	计算每个数字对应有多少种可能填入的值
int turnDigits[ALLDIGITS + 1] = { 0 };	//	把存储的数据转换到1-9的输出

//取最右侧的1
int quyu(int pos) {
	return pos & (~pos + 1);
}

void Init()
{
	for (int i = 0; i <= ALLDIGITS; i++)
	{
		calDigits[i] = (i & 1) + ((i >> 1) & 1) + ((i >> 2) & 1) + ((i >> 3) & 1)
			+ ((i >> 4) & 1) + ((i >> 5) & 1) + ((i >> 6) & 1) + ((i >> 7) & 1) + ((i >> 8) & 1);
	}
	for (int i = 0; i <= 9; i++)
	{
		turnDigits[1 << i] = i + 1;
	}
}

Sudoku::Sudoku()
{
	memset(records, 0x0, 81 * sizeof(int));
}

Sudoku::Sudoku(std::string fin)
{
	inputfile(fin);
}

Sudoku::~Sudoku()
{

}


// 传入：index 要判断的位置 m需要判断的相关索引
// 比如是第一行的话 m 就是 012345678
bool Sudoku::CalPossibility(int index, int m[])
{
	// mustHave 记录这一排已经被确定下来的数字
	// canHave 记录这一排可能存在的所有数字
	int mustHave = 0;
	int canHave = 0;
	int temp = records[index];

	for (int i = 0; i < 9; i++)
	{
		int j = m[i];
		if (index != j)
		{
			canHave |= records[j];
			if (1 == calDigits[records[j]])
			{
				mustHave |= records[j];
			}
		}
	}
	records[index] &= (mustHave^ALLDIGITS);
	if (0 == records[index])
	{
		//std::cout << "WRONG SUDOKU" << std::endl;
		return false;
	}

	if (canHave != ALLDIGITS)
	{
		// 如果 这一排加上i都不能覆盖所有可能 或者 这一排缺少多余1个数字
		// 那么说明没有正常解
		if ((canHave | records[index]) != ALLDIGITS || calDigits[canHave] != 8)
		{
			//std::cout << "WRONG SUDOKU" << std::endl;
			return false;
		}
		records[index] &= (canHave^ALLDIGITS);
	}
	return true;
}



bool Sudoku::Optimize()
{
	int reset = true;
	while (1)
	{
		reset = true;
		int a[9]; // 记录索引
		int m = 0;
		for (int i = 0; i < 81; i++)
		{
			if (1 == calDigits[records[i]])
				continue;
			int temp = records[i];

			// row
			m = 0;
			for (int j = (i / 9) * 9; j < (i / 9) * 9 + 9; j++)
			{
				a[m++] = j;
			}
			if (!CalPossibility(i, a)) return false;
			//column
			m = 0;
			for (int j = i % 9; j < 81; j = j + 9)
			{
				a[m++] = j;
			}

			if (!CalPossibility(i, a)) return false;

			// lattice

			int start = (i / 27) * 27 + i % 27 / 3 % 3 * 3; // 复杂的计算了一下第一个空
			int offset[9] = { 0,1,2,9,10,11,18,19,20 };
			for (int j = 0; j < 9; j++)
			{
				a[j] = start + offset[j];
			}

			if (!CalPossibility(i, a)) return false;

			// 如果数据更新过 则需要进行下一次循环
			if (temp != records[i])
				reset = false;
		}
		if (reset)
			break;
		//output();
		//std::cout << std::endl;
	}
	//output();
	return true;
}



Sudoku Sudoku::operator=(const Sudoku m)
{
	memcpy(records, m.records, 81 * sizeof(int));
	return *this;
}


// 读取文件，更新数据
void Sudoku::inputfile(std::string finfile)
{
	std::ifstream fin(finfile);
	int s;
	for (int i = 0; i < 81; i++)
	{
		fin >> s;
		if (s != 0)
			records[i] = (1 << (s - 1)) & ALLDIGITS;
		else
			records[i] = ALLDIGITS;
	}
}


// 主要调用的函数
int Sudoku::Solve()
{

	bool ok = Optimize();
	if (!ok) return 0;

	int max = 0, min = 10, minIndex = -1;
	int t;
	for (int i = 0; i < 81; i++)
	{
		t = calDigits[records[i]];
		if (t > max)	max = t;
		if (t < min && t > 1) {
			min = t;
			minIndex = i;
		}
	}

	// 如果都只有1个可能的数字
	if (max == 1)
		return 1;

	int changebit = quyu(records[minIndex]);

	// 测试一下是这一位和不是这一位的情况
	Sudoku branch1 = *this;
	branch1.records[minIndex] = changebit;
	int result1 = branch1.Solve();

	Sudoku branch2 = *this;
	branch2.records[minIndex] &= (changebit^ALLDIGITS);
	int result2 = branch2.Solve();


	// 处理结果
	if (result1 == 0 && result2 == 0)
		return 0;
	if (result1 == 0)
	{
		*this = branch2;
		return result2;
	}
	if (result2 == 0)
	{
		*this = branch1;
		return result1;
	}

	// 输出其中一种解
	*this = branch1;
	return result1 + result2;
}

// 输出可以填入的可能性信息
void Sudoku::outputPro(void)
{
	for (int i = 0; i < 81; i++)
	{
		std::cout << records[i] << ' ';
		if (i % 9 == 8)
			std::cout << std::endl;
	}
}


// 输出已经确认的格子
void Sudoku::output(void)
{
	for (int i = 0; i < 81; i++)
	{
		std::cout << turnDigits[records[i]] << ' ';
		if (i % 9 == 8)
			std::cout << std::endl;
	}
}