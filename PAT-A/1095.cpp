//@ Birdy&C
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int recordNumber, requreNumber;//the number of records, and  the number of queries.
const int max_recordNumber = 10001; //the max number of record
int timeStatue[86401] = { 0 }; //record the total statue in every seconds
int maxDuration = 0; //record the longest duration time

enum status_condition //the status of the record
{
    IN,//status is in
    OUT//status is out
};
struct Node {
    int index = 0;//record the index of cars in the list
    Node* next = NULL;//point to next node
}head;//record the car that has packed for the longest time
struct record{
    char plate_number[8]; //plate_number is a string of 7 English capital letters or 1-digit numbers
    int time;//represents the time point in a day ,similar to hour:minute:second
    status_condition status;//reprent the status,in & out
}Record[max_recordNumber];

int cmpPlate(const void *a, const void *b) {//sort in a decrease order because when save it into a list it is reversed
    return -strcmp(((record *)a)->plate_number, ((record *)b)->plate_number);
}
int cmpTime(const void *a, const void *b) {//sort in a accending order of time
    return ((record *)a)->time-((record *)b)->time;
}
void clear() //clear the list of car(s) staying longest
{
    Node* p,*p_temp;
    p = head.next;
    while (p != NULL) 
    {
        p_temp = p->next;
        free(p);//realse space
        p = p_temp;
    }
    head.next = NULL;
}
void insert(const int i) //record car with index i
{
    Node* pnode;
    pnode = (Node*)malloc(sizeof(struct Node));
    pnode->index = i;
    pnode->next = head.next;
    head.next = pnode;//insert it after head node
}

int getTime() 
{
    int hour, minute, second;
    scanf("%d:%d:%d", &hour, &minute, &second);//get the input
    return hour * 3600 + minute * 60 + second;//transfer it to integer

}

void countDuration(const int start, const int count) 
{
    int duration = 0;
    status_condition flag;//remember the status of the car
    int pre;
    int t;
    qsort(Record + start, count, sizeof(Record[0]), cmpTime);
    flag = OUT;
    for(int i = start; i < start + count; i++)
    {
         t = Record[i].time;
        if (IN == Record[i].status) 
        {
                flag = IN; //car is inside the campus
                pre = t;
        }
        else 
        {           
            if (OUT == flag) continue;//car is outside the campus,means in &out record are not paired
            timeStatue[pre]++; 
            duration -= pre;
            timeStatue[t]--;        
            duration += t;
            //printf("IN %d\n", pre);
            //printf("OUT %d\n", t);
            flag = OUT;
        }
    }
    if (duration > maxDuration) 
    {
        maxDuration = duration;//renew the maxDuration
        clear();//clear the record
        insert(start);//record the car
    }
    else if (duration == maxDuration)
    {
        insert(start);//record the car
    }
}

void calculateNumber() 
{
    {
        int count = 0;
        int time = 0, temp = 0;
        count = 0;//the number of cars in the campus
        time = temp = 0;
        for (int i = 0; i < requreNumber; i++) //deal with input of records
        {
            time = getTime();//get the input time
            while (temp <= time)
            {
                count += timeStatue[temp];
                temp++;
            }
            printf("%d\n", count);
        }
    }
}
void printMaxDuration() //out put the information of duration
{
    Node* p;
    p = head.next;
    while (p != NULL)
    {
        printf("%s ", Record[p->index].plate_number);//output the plate number of the car that has parked for the longest time period
        p = p->next;
    }
    printf("%02d:%02d:%02d", maxDuration / 3600, maxDuration / 60 % 60, maxDuration % 60);//output the corresponding time length.
}

int main(void) 
{
    scanf("%d %d", &recordNumber, &requreNumber);//get the number of records, and the number of queries.

    for (int i = 0; i < recordNumber; i++) //deal with input of records
    {
        scanf("%s", Record[i].plate_number);
        Record[i].time = getTime();//get the time point
        char status_temp[4];
        scanf("%s", status_temp);
        if (status_temp[0] == 'i') Record[i].status = IN;//if status is in 
        else Record[i].status = OUT;//if status is out
    }

    qsort(Record, recordNumber,sizeof(Record[0]),cmpPlate);//separte the record according to palte number

        int temp = 0;
        int count = 0;//count the number of record of a certain car
        char plate[8];//remember the plate number of the car
        while (temp < recordNumber)
        {
            count = 0;
            strcpy(plate, Record[temp].plate_number);
            while (0 == strcmp(plate, Record[temp].plate_number))//count the information of a certain car
            {
                temp++;
                count++;
            }
                countDuration(temp - count, count); //deal with the information
        }


    calculateNumber();  //calculate the total number of cars parking on campus 
    printMaxDuration(); //out put the information of duration
    clear();//realse the space
    system("pause");
    return 0;
}