/* 
@author Birdy&C`
*/
#include <iostream> 
#include <string>
#include <map>

using namespace std;
int total_Hour = 0, total_Number = 0;
string Promblem_name[9];
int problem[9][2];
int max_count = 0;
int min_time;
int temp[9] = { 0 };

int result[9] = { 0 };
map<int, int> to_int;


void copy() 
{
	for (int i = 0; i < total_Number; i++)
	{
		result[i] = temp[i];
	}
}



bool Backtracking(const int in_list,const int search_list,const int time_spent,const int all_time, const int count)
{
	if (time_spent > total_Hour)
	{
		return true;
	}
	if (count > max_count) //solve more problem
	{
		max_count = count;
		min_time = all_time;
		copy();
	}
	else if (count == max_count && all_time < min_time) //use less time
	{
		min_time = all_time;
		copy();
	}

	int check = search_list;
	while (check) 	
	{
		int add = check&(~check + 1);
		check = check&(~add);
		int reject;
		int time;
		reject = (time_spent + problem[to_int[add]][0] - 1) / 60;
		time = problem[to_int[add]][0] + reject*(problem[to_int[add]][1]);
		temp[count] = to_int[add];    
		if (time_spent + time <= total_Hour)
		{
			Backtracking(in_list | add, search_list & ~add, time_spent + time,
				all_time + time_spent + time + 20 * reject, count + 1);
		}
	}
	return true;
}

int main() 
{
	//Init
	int read_time;

	cin >> total_Hour;
	while (total_Hour >= 0)
	{

		cin >> total_Number >> read_time;
		if (total_Number > 9 || total_Number < 0) return 0;
		total_Hour *= 60;
		min_time = 0;
		max_count = 0;

		int search_list = 0;

		for (int i = 0; i < total_Number; i++)
		{
			cin >> Promblem_name[i] >> problem[i][0] >> problem[i][1];
			search_list += 1 << i;
			to_int[1 << i] = i;
		}


		//backtracking-findmin

		Backtracking(0, search_list, read_time, 0, 0);


		//print
		cout << "Total Time = " << min_time  << endl;
		for (int i = 0; i < max_count; i++)
		{
			cout << Promblem_name[result[i]] << endl;
		}



		//new cycle
		cin >> total_Hour;
	}
	
	system("pause");
	return 0;
}