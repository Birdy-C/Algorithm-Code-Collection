#pragma once
void Init();

class Sudoku
{
	// 记录方式为 有这个可能性的位为1
	int records[81];
	bool CalPossibility(int i, int m[]);
	bool Optimize();

public:

	Sudoku();
	Sudoku(std::string fin);
	~Sudoku();
	Sudoku operator=(const Sudoku m);
	void inputfile(std::string finfile);
	int Solve();
	void outputPro(void);
	void output(void);
};
