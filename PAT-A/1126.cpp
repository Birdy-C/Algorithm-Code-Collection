// @author Birdy 2018.3.5
/*
1126
Point 4 N = 1
Point 5 N = 2
*/
#include<iostream>
#include<algorithm>

using namespace std;


int main()
{
	int N, M;
	int num[502] = { 0 };
	int root[502];
	cin >> N >> M;
	int H1, H2;
	for (int i = 0; i < N; i++)
	{
		root[i] = i;
	}
	for (int i = 0; i < M; i++)
	{
		cin >> H1 >> H2;
		num[H1 - 1]++;
		num[H2 - 1]++;
		root[H1 - 1] = min(root[H1 - 1], H2 - 1);
		root[H2 - 1] = min(root[H2 - 1], H1 - 1);
	}

	int flag = 0;

	for (int i = 0; i < N; i++)
	{
		if (0 == i)
			cout << num[i];
		else
		{
			cout << ' ' << num[i];
			if (i == root[i])
				flag = 3;


		}

		if (num[i] & 1 == 1)
			flag++;
		else if (0 == num[i])
			flag = 3;

	}

	cout << endl;

	if (flag == 0 || 1 == N)
		cout << "Eulerian" << endl;
	else if (flag == 2)
		cout << "Semi-Eulerian" << endl;
	else
		cout << "Non-Eulerian" << endl;
	system("pause");
	return 0;
}
