// @author Birdy 2018.2.26
/*
Test point 1&2  0000 -0000
Test point 3&4 same gender test case
*/
#include<iostream>
#include<vector>
#include<map>
#include<cassert>
#include<string>
#include<algorithm>

using namespace std;

class relation
{
	string index;
	vector<string> samegender;
	vector<string> difgender;
public:
	relation(string index_t):index(index_t) {};
	void addfriend(string newfriend);
	bool checkrelation(string relation);
	vector<string> samegenderfriend() { return samegender; };
	string getindex() { return index; }
	//vector<int> difgenderfriend() { return difgender; };

};

class friendpair //record the output
{
public:
	int friend1;
	int friend2;
	friendpair(int f1, int f2) :friend1(f1), friend2(f2) {}
	void printpair();
};


class person
{
	map<string, class relation> index; // map person id to index
public:
	person() {};
	void addrelation(const string person1, const string person2);
	void addperson(const string id);
	map<string, class relation>::iterator findperson(const string id);
	vector<friendpair> findrelation(const string person1, const string person2);
};


void relation::addfriend(string newfriend)
{
	if ((index[0] == '-') == (newfriend[0] == '-'))
	{
		samegender.push_back(newfriend);
	}
	else
	{
		difgender.push_back(newfriend);
	}
}

bool relation::checkrelation(string relation)
{
	vector<string>::iterator findrelation;
	if ((index[0] == '-') == (relation[0] == '-'))
	{
		findrelation = std::find(samegender.begin(), samegender.end(), relation);
		if (findrelation == samegender.end())
			return false;
		else
			return true;
	}
	else
	{
		findrelation = std::find(difgender.begin(), difgender.end(), relation);
		if (findrelation == difgender.end())
			return false;
		else
			return true;
	}
	return false;
}


void person::addperson(const string id)
{
	index.insert(pair<string, class relation>(id, relation(id)));
}

map<string, class relation>::iterator person::findperson(const string id)
{
	map<string, class relation>::iterator it = index.find(id);
	if (it != index.end())
	{
		return it;
	}
	addperson(id);
	it = index.find(id);
	assert(it != index.end());
	return it;
}

vector<friendpair> person::findrelation(const string person1, const string person2)
{
	vector<friendpair> relation;
	vector<string> friend1 = (findperson(person1)->second).samegenderfriend();
	vector<string> friend2 = (findperson(person2)->second).samegenderfriend();

	for (vector<string>::iterator it1 = friend1.begin(); it1 < friend1.end(); it1++)
	{
		class relation friend_of_1 = findperson(*it1)->second;
		for (vector<string>::iterator it2 = friend2.begin(); it2 < friend2.end(); it2++)
		{
			if (friend_of_1.checkrelation(*it2))
			{
				if (person1 == *it2 || person2 == friend_of_1.getindex() || *it2 == friend_of_1.getindex()) continue;
				relation.push_back(friendpair(abs(atoi(friend_of_1.getindex().c_str())), abs(atoi((*it2).c_str()))) );//abs - output don't have -
				//cout << friend_of_1.getindex() << *it2 << endl;
			}
		}

	}
	//sort
	std::sort(relation.begin(), relation.end());
	return relation;
}

void person::addrelation(const string person1, const string person2)
{
	map<string, class relation>::iterator it1 = findperson(person1);
	map<string, class relation>::iterator it2 = findperson(person2);
	it1->second.addfriend(person2);
	it2->second.addfriend(person1);
}



bool operator<(const friendpair &s1, const friendpair &s2)
{
	return s1.friend1 == s2.friend1 ? s1.friend2 < s2.friend2 : s1.friend1 < s2.friend1;
}

void friendpair::printpair()
{
	//cout << friend1 << ' ' << friend2 << endl;
	printf("%04d %04d\n", friend1, friend2);
}

int main()
{
	int N, M, K;
	// what's the use of N?
	cin >> N >> M;

	person Person;
	int friend1, friend2;
	string IDstring1, IDstring2;
	// Test point 1 2 input contains 0
	for (int i = 0; i < M; i++)
	{
		cin >> IDstring1 >> IDstring2;
		Person.addrelation(IDstring1, IDstring2);
	}
	cin >> K;
	for (int i = 0; i < K; i++)
	{
		cin >> IDstring1 >> IDstring2;

		vector<friendpair> result = Person.findrelation(IDstring1, IDstring2);
		cout << result.size() << endl;
		for (vector<friendpair>::iterator it = result.begin(); it < result.end(); it++)
		{
			it->printpair();
		}

	}
	system("pause");
	return 0;
}
